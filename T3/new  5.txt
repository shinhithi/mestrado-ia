--0.05
=== Run information ===

Scheme:weka.classifiers.trees.J48 -C 0.05 -M 2
Relation:     eleicoes-ms-mt
Instances:    466
Attributes:   8
              politico
              homem
              superior
              solteiro
              faixa1
              faixa2
              faixa3
              class
Test mode:10-fold cross-validation

=== Classifier model (full training set) ===

J48 pruned tree
------------------

faixa2 = A: 20Mais (117.0/28.0)
faixa2 = B: NAO (147.0/3.0)
faixa2 = C: NAO (87.0/1.0)
faixa2 = D: NAO (115.0)

Number of Leaves  : 	4

Size of the tree : 	5


Time taken to build model: 0.01 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances         434               93.133  %
Incorrectly Classified Instances        32                6.867  %
Kappa statistic                          0.804 
Mean absolute error                      0.1086
Root mean squared error                  0.2338
Relative absolute error                 33.8963 %
Root relative squared error             58.4874 %
Total Number of Instances              466     

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.957     0.075      0.761     0.957     0.848      0.928    20Mais
                 0.925     0.043      0.989     0.925     0.956      0.928    NAO
Weighted Avg.    0.931     0.049      0.943     0.931     0.934      0.928

=== Confusion Matrix ===

   a   b   <-- classified as
  89   4 |   a = 20Mais
  28 345 |   b = NAO

--  0.2
Relative absolute error                 26,4867 %
Relative absolute error                 30,6557 %
--  0,3
Relative absolute error                 26,4867 %  
Relative absolute error                 28,7342 %
-- 0,4
Relative absolute error                 24,7507 %
Relative absolute error                 28,1388 %
-- 0,5
Relative absolute error                 24,7507 %
Relative absolute error                 27,8876 %
--0,6
Relative absolute error                 23,1075 %
Relative absolute error                 26,9076 %
-- 0,7
Relative absolute error                 23,1075 %
Relative absolute error                 26,9076 %
-- 1
Relative absolute error                 23,1075 %
Relative absolute error                 26,9076 %
----------------------------------------------------------------------------------------------------------------------  
1.0E-8
1.0E-12
=== Summary ===

Correctly Classified Instances         442               94.8498 %
Incorrectly Classified Instances        24                5.1502 %
Kappa statistic                          0.845 
Mean absolute error                      0.0883
Root mean squared error                  0.2153
Relative absolute error                 27.5711 %
Root relative squared error             53.8578 %
Total Number of Instances              466     

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.925     0.046      0.835     0.925     0.878      0.962    20Mais
                 0.954     0.075      0.981     0.954     0.967      0.962    NAO
Weighted Avg.    0.948     0.069      0.952     0.948     0.949      0.962

=== Confusion Matrix ===

   a   b   <-- classified as
  86   7 |   a = 20Mais
  17 356 |   b = NAO

  
=== Summary ===

Correctly Classified Instances         434               93.133  %
Incorrectly Classified Instances        32                6.867  %
Kappa statistic                          0.804 
Mean absolute error                      0.1086
Root mean squared error                  0.2338
Relative absolute error                 33.8965 %
Root relative squared error             58.4874 %
Total Number of Instances              466     

=== Detailed Accuracy By Class ===

               TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
                 0.957     0.075      0.761     0.957     0.848      0.927    20Mais
                 0.925     0.043      0.989     0.925     0.956      0.927    NAO
Weighted Avg.    0.931     0.049      0.943     0.931     0.934      0.927

=== Confusion Matrix ===

   a   b   <-- classified as
  89   4 |   a = 20Mais
  28 345 |   b = NAO
  
--0.5
Relative absolute error                 27.6911 %  
Relative absolute error                 29.4717 %
--0.01
Relative absolute error                 26.1197 %
Relative absolute error                 27.6626 %
--1.0E-6
Relative absolute error                 26.0703 %
Relative absolute error                 27.5711 %
--1.0E-8
Relative absolute error                 26.0703 %
Relative absolute error                 27.5711 %
--1.0E-12
Relative absolute error                 26.0703 %
Relative absolute error                 27.5711 %
----------------------------------------------------------------------------------------------------------------------   
linear
-- c = 1	
  Relative absolute error                 24.1148 %
  85   8 |   a = 20Mais
  Relative absolute error                 21.4421 %
  
-- C=10
Relative absolute error                 24.7846 %
	84   9 |   a = 20Mais
	Relative absolute error                 21.4421 %
	
-- C=47
Relative absolute error                 24.7846 %
	84   9 |   a = 20Mais
	Relative absolute error                 21.4421 %	
	
--C=100	
83  10 |   a = 20Mais
Relative absolute error                 25.4545 %	
Relative absolute error                 21.4421 %
  
--C=777
Relative absolute error                 25.4545 %	
Relative absolute error                 21.4421 %

polynomial
-- c = 1  / D = 3
  85   8 |   a = 20Mais
  Relative absolute error                 26.1243 %
  Relative absolute error                 20.772  %
-- c = 7    / D = 3
  86   7 |   a = 20Mais  
  Relative absolute error                 20.7655 %
  Relative absolute error                 17.4217 %
-- c = 17    / D = 3  
Relative absolute error                 20.7655 %  
  85   8 |   a = 20Mais
  Relative absolute error                 14.7414 %
-- c = 43  / D = 3
Relative absolute error                 19.4258 %  
Relative absolute error                 14.0714 %
    83  10 |   a = 20Mais
-- c = 77  / D = 3	
Relative absolute error                 20.7655 %
  83  10 |   a = 20Mais	
Relative absolute error                 13.4013 %
-- c = 97  / D = 3	
Relative absolute error                 18.7559 %
Relative absolute error                 13.4013 %
-- c = 133  / D = 3	
Relative absolute error                 13.4013 %
Relative absolute error                 20.7655 %
---------------------------------------	
-- c = 1  / D = 1
Relative absolute error                 21.4354 %
  89   4 |   a = 20Mais
Relative absolute error                 21.4421 %
-- c = 7  / D = 1  
Relative absolute error                 23.4449 %
Relative absolute error                 21.4421 %
  86   7 |   a = 20Mais
-- c = 37  / D = 1    
  Relative absolute error                 24.7846 %
    84   9 |   a = 20Mais
	Relative absolute error                 21.4421 %
-- c = 57 / d = 1
	Relative absolute error                 21.4421 %
Relative absolute error                 24.7846 %	
	
-- c = 7 / d = 7	
Relative absolute error                 36.842  %	
Relative absolute error                 32.1631 %
 91   2 |   a = 20Mais

-- c = 1 / d = 5
 Relative absolute error                 36.842  %
 Relative absolute error                 34.8434 %
-- c = 7 / d = 5
Relative absolute error                 30.1435 %
 Relative absolute error                 23.4523 %
  88   5 |   a = 20Mais
-- c = 37 / d = 5
Relative absolute error                 24.1148 %
Relative absolute error                 16.7516 %
  86   7 |   a = 20Mais
-- c = 57 / d = 5  
Relative absolute error                 21.4354 %  
  86   7 |   a = 20Mais
Relative absolute error                 16.7516 %
-- c = 133 / d = 5  
Relative absolute error                 15.4115 %
Relative absolute error                 22.1052 %
-- c = 233 / d = 5  
Relative absolute error                 12.7312 %
Relative absolute error                 22.7751 %
-- c = 233 / d = 5    
Relative absolute error                 12.0612 %
Relative absolute error                 20.7655 %
  
-- c = 1  / D = 7 
  88   5 |   a = 20Mais
  Relative absolute error                 41.531  %
  Relative absolute error                 36.1835 %  
  
RBF
-- c = 1 / g = 0
Relative absolute error                 20.7655 %
  87   6 |   a = 20Mais
-- c = 1 / g = 1
Relative absolute error                 20.0956 %
79  14 |   a = 20Mais
Relative absolute error                 18.0917 %
-- c = 1 / g = 0.5
  84   9 |   a = 20Mais
Relative absolute error                 17.4162 %
Relative absolute error                 14.0714 %
-- c = 1 / g = 0.3
Relative absolute error                 18.0861 %
  84   9 |   a = 20Mais
Relative absolute error                 14.7414 %  

-- c = 7 / g = 0.5  
Relative absolute error                 18.7559 %
  82  11 |   a = 20Mais
  Relative absolute error                 12.0612 %
  
-- c = 37 / g = 0.5    
Relative absolute error                 17.4162 %  
83  10 |   a = 20Mais
Relative absolute error                 12.0612 %

-- c = 57 / g = 0.5    
Relative absolute error                 17.4162 %
Relative absolute error                 12.0612 %