import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sun.servicetag.SystemEnvironment;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Created by User on 27/08/2014.
 */
public class Main {

    private static Namespace parsedArgs;
    private static boolean verbose = false;

    public static void main(String[] args) throws IOException {

        parsedArgs = parseArgs(args);
        verbose = parsedArgs.getBoolean("verbose");

        List<BigDecimal> X1 = getX1(parsedArgs);
        int numeroExemplos = X1.size();
        List<BigDecimal> X0 = getX0(numeroExemplos);
        List<BigDecimal> Y = getY(parsedArgs);
        BigDecimal[] teta = getTeta(parsedArgs);
        BigDecimal alfa = getAlpha(parsedArgs);
        BigDecimal tetaZeroAtual;
        BigDecimal tetaUmAtual;
        BigDecimal gradiente ;
        BigDecimal erroConvergencia = getErroConvergencia(parsedArgs);
        List<Double> erros = new ArrayList<>();

        int numInteracoes = 0;
        do {

            gradiente = calculaGradiente(X0, numeroExemplos, teta, X0, X1, Y);
            tetaZeroAtual = calculaTeta(teta[0], alfa, gradiente);

            gradiente = calculaGradiente(X1, numeroExemplos, teta, X0, X1, Y);
            tetaUmAtual = calculaTeta(teta[1], alfa, gradiente);

            numInteracoes++;
            teta[0] = tetaZeroAtual;
            teta[1] = tetaUmAtual;

            Double erro = costFunction(teta, X0, X1, Y).setScale(4, RoundingMode.HALF_UP).doubleValue();
            erros.add(erro);
            if (verbose && numInteracoes % 100 == 0) {
                System.out.println("Interação número " + (numInteracoes));
                System.out.println("Gradiente 1 = " + gradiente.setScale(4, RoundingMode.HALF_UP));
                System.out.println("Teta 0 = " + tetaZeroAtual.setScale(4, RoundingMode.HALF_UP));
                System.out.println("Teta 1 = " + tetaUmAtual.setScale(4, RoundingMode.HALF_UP));
                System.out.println("J(0,1) = " + erro);
            }

        } while (convergiu(gradiente, erroConvergencia, numInteracoes));


        if(verbose) {
            for(int i =0; i < X1.size(); i++){
                BigDecimal x =  calculaHipotese(i, teta, X0, X1);
                System.out.println("X=" + x.setScale(2, RoundingMode.HALF_UP) + " Y=" + Y.get(i).setScale(2, RoundingMode.HALF_UP));
            }
        }

        System.out.println("Número de Interações = " + (numInteracoes));
        System.out.println("Alpha = " + alfa.setScale(8, RoundingMode.HALF_UP));
        System.out.println("Resultado => Teta 0 = " + tetaZeroAtual.setScale(4, RoundingMode.HALF_UP));
        System.out.println("Resultado => Teta 1 = " + tetaUmAtual.setScale(4, RoundingMode.HALF_UP));

        geraImagemGraficoErro(erros);

        geraImagemGraficoPredicao(X1, numeroExemplos, X0, Y, teta);

        //costFunction(teta, X0, X1, Y).setScale(4, RoundingMode.HALF_UP).doubleValue();
        //double t0_inc = 0.1;
        //double t1_inc = 0.008;
        double t0_inc = 0.8;
        double t1_inc = 0.008;
        double t0 = 1;
        double t1 = 0.2;
        for(int i =1; i <= 25; i++ ){
            t0 = 1;
            System.out.println("T1 - " + t1);
            for(int j=1; j<= 25; j++){
                teta[0] = BigDecimal.valueOf(t0);
                teta[1] = BigDecimal.valueOf(t1);
                double val = costFunction(teta, X0, X1, Y).setScale(4, RoundingMode.HALF_UP).doubleValue();
                System.out.println(t0 + ";" + val);
                t0 += t0_inc;
            }
            t1 += t1_inc;
        }
        System.out.println(t0);
        System.out.println(t1);
    }

    private static BigDecimal costFunction(BigDecimal[] teta, List<BigDecimal> X0, List<BigDecimal> X1, List<BigDecimal> Y){
        BigDecimal somatorio = BigDecimal.ZERO;
        for(int i = 0; i <  X1.size(); i++){
            somatorio = somatorio.add( (calculaHipotese(i, teta, X0, X1).subtract(Y.get(i)).pow(2)));
        }
        return somatorio.divide(BigDecimal.valueOf(2 * X1.size()), 20, RoundingMode.HALF_UP);
    }

    private static BigDecimal calculaTeta(BigDecimal teta, BigDecimal alfa, BigDecimal erro) {
        return (teta.subtract(( alfa.multiply(erro))));
    }

    private static boolean convergiu(BigDecimal erro, BigDecimal erroConvergencia, int numInteracoes) {
        //|| numInteracoes == 1196
        return erro.abs().compareTo(erroConvergencia) > 0;
    }

    private static BigDecimal calculaGradiente(List<BigDecimal> ex, int numeroExemplos, BigDecimal[] teta, List<BigDecimal> x0, List<BigDecimal> x1, List<BigDecimal> y) {

        BigDecimal somatorio = BigDecimal.ZERO;
        for(int i = 0; i <  x0.size(); i++){
            somatorio = somatorio.add((calculaHipotese(i, teta, x0, x1).subtract(y.get(i))).multiply(ex.get(i)));
        }

        return somatorio.divide(BigDecimal.valueOf(numeroExemplos), 20, RoundingMode.HALF_UP);
    }

    private static BigDecimal calculaHipotese(int idx, BigDecimal[] teta, List<BigDecimal> x0, List<BigDecimal> x1) {
        return teta[0].multiply(x0.get(idx)).add(teta[1].multiply(x1.get(idx)));
    }

    private static void geraImagemGraficoErro(List<Double> erros) throws IOException {

        if( parsedArgs.getBoolean("imagem-grafico") == null ) return;

        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        for (int i = 0; i < erros.size(); i = i + 2) {
            ds.addValue(erros.get(i) * 100, "erro", "interacao " + i);
        }

        // cria o gráfico
        JFreeChart grafico = ChartFactory.createLineChart("Erro", "Interações",
                "Valor", ds, PlotOrientation.VERTICAL, true, true, false);

        OutputStream arquivo = new FileOutputStream("grafico_erro.png");
        ChartUtilities.writeChartAsPNG(arquivo, grafico, 600, 400);
        arquivo.close();

    }

    private static void geraImagemGraficoPredicao(List<BigDecimal> x1, int numeroExemplos, List<BigDecimal> x0, List<BigDecimal> y, BigDecimal[] teta) throws IOException {

        if( parsedArgs.getBoolean("imagem-grafico") == null ) return;

        XYSeries series1 = new XYSeries("Predição");
        for(int i = 0; i < numeroExemplos; i++) {
            series1.add( x1.get(i).setScale(2, RoundingMode.HALF_UP).doubleValue(),
                    calculaHipotese(i, teta, x0, x1).setScale(2, RoundingMode.HALF_UP).doubleValue()  );
        }
        XYSeries series2 = new XYSeries("Exemplo Y");
        for(int i = 0; i < numeroExemplos; i++) {
            series2.add(x1.get(i).setScale(2, RoundingMode.HALF_UP).doubleValue(),
                    y.get(i).setScale(2, RoundingMode.HALF_UP).doubleValue());
        }
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);

        JFreeChart grafico = ChartFactory.createXYLineChart(
                "Gráfico Predição X vs Exemplo Y",
                "X",
                "Y",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                false,
                false
        );

        OutputStream arquivo = new FileOutputStream("grafico_x-y.png");
        ChartUtilities.writeChartAsPNG(arquivo, grafico, 600, 400);
        arquivo.close();
    }

    private static BigDecimal[] getTeta(Namespace parsedArgs) {
        BigDecimal[] d = new BigDecimal[2];

        d[0] = BigDecimal.ZERO;
        if(parsedArgs.getString("T0") != null){
            d[0] = new BigDecimal(parsedArgs.getString("T0"));
        }

        d[1] = BigDecimal.ZERO;
        if(parsedArgs.getString("T1") != null){
            d[1] = new BigDecimal(parsedArgs.getString("T1"));
        }
        return d;
    }

    private static BigDecimal getAlpha(Namespace parsedArgs) {
        if(parsedArgs.getString("A") != null){
            return new BigDecimal(parsedArgs.getString("A"));
        }
        return new BigDecimal("0.01");

    }

    private static List<BigDecimal> getX0(int numeroExemplos) {
        List<BigDecimal> X0 = new ArrayList<>();
        for(int i =0; i < numeroExemplos; i++){
            X0.add(BigDecimal.ONE);
        }
        return X0;
    }

    private static List<BigDecimal> getX1(Namespace parsedArgs) throws IOException {
        if(parsedArgs.getString("X") != null){
            List<BigDecimal> X1 = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(parsedArgs.getString("X")));
            while(br.ready()){
                X1.add(new BigDecimal(br.readLine().trim()));
            }
            br.close();
            return X1;
        }
        return Arrays.asList(BigDecimal.valueOf(1),BigDecimal.valueOf(2),BigDecimal.valueOf(3),BigDecimal.valueOf(4),BigDecimal.valueOf(5),BigDecimal.valueOf(6),BigDecimal.valueOf(7),BigDecimal.valueOf(8),BigDecimal.valueOf(9));
    }

    private static List<BigDecimal> getY(Namespace parsedArgs) throws IOException {
        if(parsedArgs.getString("Y") != null){
            List<BigDecimal> Y1 = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(parsedArgs.getString("Y")));
            while(br.ready()){
                Y1.add(new BigDecimal(br.readLine().trim()));
            }
            br.close();
            return Y1;
        }
        return Arrays.asList(BigDecimal.valueOf(3),BigDecimal.valueOf(5),BigDecimal.valueOf(7),BigDecimal.valueOf(9),BigDecimal.valueOf(11),BigDecimal.valueOf(13),BigDecimal.valueOf(15),BigDecimal.valueOf(17),BigDecimal.valueOf(19));
    }

    private static BigDecimal getErroConvergencia(Namespace parsedArgs) {
        if(parsedArgs.getString("E") != null){
            return new BigDecimal(parsedArgs.getString("E"));
        }
        return new BigDecimal("0.000001");
    }

    private static Namespace parseArgs(String[] args)
    {
        ArgumentParser parser = ArgumentParsers
                .newArgumentParser("Main")
                .defaultHelp(true)
                .description(
                        "Realiza a regrecao linear para uma variavel");

        parser.addArgument("-X")
                .help("Caminho para o Arquivo de exemplo de 1 variavel")
                .required(false);

        parser.addArgument("-Y")
                .help("Caminho para o Arquivo de exemplo do valor esperado")
                .required(false);

        parser.addArgument("-T0")
                .help("Teta zero inicial(default - 0)")
                .required(false);

        parser.addArgument("-T1")
                .help("Teta um inicial(default - 0)")
                .required(false);

        parser.addArgument("-A")
                .help("Alpha inicial(default - 0.01)")
                .required(false);

        parser.addArgument("-E")
                .help("Erro convergencia(default - 0.000001)")
                .required(false);

        parser.addArgument("--verbose").dest("verbose").action(Arguments.storeConst()).setConst(true).setDefault(false)
                .type(Boolean.class).help("Verbose mode");

        parser.addArgument("--imagem-grafico").dest("imagem-grafico").action(Arguments.storeConst()).setConst(true).setDefault(false)
                .type(Boolean.class).help("Gerar imagem dos gráficos");

        try
        {
            return parser.parseArgs(args);
        }
        catch (ArgumentParserException e)
        {
            parser.handleError(e);
            System.exit(-1);
            return null;
        }
    }

}